const express = require('express');
const bodyParser = require('body-parser');
const mysql = require('mysql');

const app = express();
const PORT = 8000;


app.get('/', (req, res) => {
    res.sendFile(__dirname + '/index.html');
});

app.use(bodyParser.json());

const db = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'root',
    database: 'tp1_nodejs'
});

db.connect((err) => {
    if (err) {
        console.error('Erreur de connexion à la db');
        return;
    }
    console.log('Connecté à la db ');
});

app.listen(PORT, () => {
    console.log(`Serveur executé sur le port ${PORT}`);
});


app.get('/utilisateurs', (req, res) => {
    db.query('SELECT * FROM utilisateur', (err, result) => {
        if (err) throw err;
        res.json(result);
    });
});

app.post('/utilisateurs', (req, res) => {
    const utilisateur = req.body;
    db.query('INSERT INTO utilisateur SET id = DEFAULT, ?', utilisateur, (err, result) => {
        if (err) throw err;
        res.json(utilisateur);
    });
});

app.put('/utilisateurs/:id', (req, res) => {
    const id = req.params.id;
    const utilisateur = req.body;
    db.query('UPDATE utilisateur SET ? WHERE id = ?', [utilisateur, id], (err, result) => {
        if (err) throw err;
        res.json(utilisateur);
    });
});

app.delete('/utilisateurs/:id', (req, res) => {
    const id = req.params.id;
    db.query('DELETE FROM utilisateur WHERE id = ?', id, (err, result) => {
        if (err) throw err;
        res.send('Utilisateur supprimé avec succès.');
    });
});

app.post('/technologies', (req, res) => {
    const technologie = req.body;
    db.query('INSERT INTO technologie SET id = DEFAULT, ?', technologie, (err, result) => {
        if (err) throw err;
        res.json(technologie);
    });
});

app.post('/commentaires', (req, res) => {
    const commentaire = req.body;
    db.query('INSERT INTO commentaire SET id = DEFAULT, ?', commentaire, (err, result) => {
        if (err) throw err;
        res.json(commentaire);
    });
});

app.get('/commentaires/:technologie_id', (req, res) => {
    const technologieId = req.params.technologie_id;
    db.query('SELECT * FROM commentaire WHERE technologie_id = ?', technologieId, (err, result) => {
        if (err) throw err;
        res.json(result);
    });
});

app.get('/messages/:utilisateur_id', (req, res) => {
    const utilisateurId = req.params.utilisateur_id;

    db.query('SELECT * FROM commentaire WHERE utilisateur_id = ?', utilisateurId, (err, commentaires) => {
        if (err) throw err;

        const dateLimite = req.query.date;

        if (dateLimite) {
            db.query('SELECT * FROM commentaire WHERE utilisateur_id = ? AND date_commentaire < ?', [utilisateurId, dateLimite], (err, result) => {
                if (err) throw err;
                res.json(result);
            });
        } else {
            res.json(commentaires);
        }
    });
});

app.get('/messages/utilisateur/:utilisateur_id', (req, res) => {
    const utilisateurId = req.params.utilisateur_id;

    db.query('SELECT * FROM commentaire WHERE utilisateur_id = ?', utilisateurId, (err, result) => {
        if (err) throw err;
        res.json(result);
    });
});

app.get('/commentaires/antérieurs/:date_limite', (req, res) => {
    const dateLimite = req.params.date_limite;

    db.query('SELECT * FROM commentaire WHERE date_commentaire < ?', dateLimite, (err, result) => {
        if (err) throw err;
        res.json(result);
    });
});