const express = require('express');
const bodyParser = require('body-parser');
const mysql = require('mysql');
const bcrypt = require('bcrypt');
const utilisateurController = require('../controllers/utilisateurController')


router.get('/utilisateurs', utilisateurController.getAllUtilisateur)
router.post('/utilisateurs', utilisateurController.postUtilisateur)
router.get('/utilisateurs/:id', utilisateurController.putUtilisateur)
router.post('/utilisateurs/:id', utilisateurController.deleteUtilisateur)
router.post('/inscription', utilisateurController.postConnexion)
router.post('/connexion', utilisateurController.postInscription)


module.exports = router