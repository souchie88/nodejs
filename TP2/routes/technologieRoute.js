const express = require('express');
const bodyParser = require('body-parser');
const mysql = require('mysql');
const bcrypt = require('bcrypt');
const technologieController = require('../controllers/technologieController')

router.get('/technologies', technologieController.getAllTechnologies)
router.post('/technologies', technologieController.postTechnologie)
router.get('/technologies/:id', technologieController.putTechnologie)
router.post('/technologies/:id', technologieController.deleteTechnologie)


module.exports = router