const bodyParser = require('body-parser');
const mysql = require('mysql');
const bcrypt = require('bcrypt');
const express = require('express')
const app = express()
const cors = require('cors')

app.get('/', (req, res) => {
    res.sendFile(__dirname + '/index.html');
});

app.use(bodyParser.json());

const utilisateurRoute = require('./routes/utilisateurRoute')
const commentaireRoute = require('./routes/commentaireRoute')
const technologieRoute = require('./routes/technologieRoute')

app.use('/utilisateur', utilisateurRoute)
app.use('/commentaire', commentaireRoute)
app.use('/technologieRoute', technologieRoute)


app.listen(8000, function(){
    console.log("serveur ouvert sur le port 8000");
})