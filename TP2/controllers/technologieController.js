const db = require('../database/database')

exports.getAllTechnologies('/technologies', (req, res) => {
    db.query('SELECT id, nom_techno, date_creation, nom_createur FROM technologie', (err, result) => {
        if (err) throw err;
        res.json(result);
    });
});

exports.postTechnologie('/technologies', (req, res) => {
    const technologie = req.body;
    technologie.nom_createur = "temopraireCreateur";
    db.query('INSERT INTO technologie SET id = DEFAULT, ?', technologie, (err, result) => {
        if (err) throw err;
        res.json(technologie);
    });
});

exports.putTechnologie('/technologies/:id', (req, res) => {
    const id = req.params.id;
    const technologie = req.body;
    technologie.nom_createur = "temopraireCreateur";
    db.query('UPDATE technologie SET ? WHERE id = ?', [technologie, id], (err, result) => {
        if (err) throw err;
        res.json(technologie);
    });
});

exports.deleteTechnologie('/technologies/:id', (req, res) => {
    const id = req.params.id;
    db.query('DELETE FROM technologie WHERE id = ?', id, (err, result) => {
        if (err) throw err;
        res.send('Technologie supprimée');
    });
});