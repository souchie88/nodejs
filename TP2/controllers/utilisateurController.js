const db = require('../database/database')

exports.getAllUtilisateur('/utilisateurs', (req, res) => {
    db.query('SELECT * FROM utilisateur', (err, result) => {
        if (err) throw err;
        res.json(result);
    });
});

exports.postUtilisateur('/utilisateurs', (req, res) => {
    const utilisateur = req.body;
    db.query('INSERT INTO utilisateur SET id = DEFAULT, ?', utilisateur, (err, result) => {
        if (err) throw err;
        res.json(utilisateur);
    });
});

exports.putUtilisateur('/utilisateurs/:id', (req, res) => {
    const id = req.params.id;
    const utilisateur = req.body;
    db.query('UPDATE utilisateur SET ? WHERE id = ?', [utilisateur, id], (err, result) => {
        if (err) throw err;
        res.json(utilisateur);
    });
});

exports.deleteUtilisateur('/utilisateurs/:id', (req, res) => {
    const id = req.params.id;
    db.query('DELETE FROM utilisateur WHERE id = ?', id, (err, result) => {
        if (err) throw err;
        res.send('Utilisateur supprimé');
    });
});



exports.postConnexion('/inscription', async (req, res) => {
    const { nom, prenom, email, password } = req.body;

    const existingUser = await db.query('SELECT * FROM utilisateur WHERE email = ?', [email]);
    if (existingUser.length > 0) {
        return res.status(409).json({ message: 'Cet email est déjà utilisé.' });
    }

    const hashedPassword = await bcrypt.hash(password, 10);

    await db.query('INSERT INTO utilisateur SET ?', {
        nom: nom,
        prenom: prenom,
        email: email,
        Password: hashedPassword,
        Role: 'utilisateur'
    });
});

exports.postInscription('/connexion', async (req, res) => {
    const { email, password } = req.body;

    try {
        const user = await db.query('SELECT * FROM utilisateur WHERE email = ?', [email]);

        if (user.length === 0) {
            return res.status(401).json({ message: 'Adresse email ou mot de passe incorrect.' });
        }

        const passwordMatch = await bcrypt.compare(password, user[0].Password);
        if (!passwordMatch) {
            return res.status(401).json({ message: 'Adresse email ou mot de passe incorrect.' });
        }

        res.status(200).json({ message: 'Connexion réussie' });
    } catch (error) {
        console.error(error);
        res.status(500).json({ message: 'Erreur lors de la connexion.' });
    }
});